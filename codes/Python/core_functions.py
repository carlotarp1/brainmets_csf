def rand_cmap(nlabels, type='bright', first_color_black=True, last_color_black=False, verbose=True):
    """
    Creates a random colormap to be used together with matplotlib. Useful for segmentation tasks
    :param nlabels: Number of labels (size of colormap)
    :param type: 'bright' for strong colors, 'soft' for pastel colors
    :param first_color_black: Option to use first color as black, True or False
    :param last_color_black: Option to use last color as black, True or False
    :param verbose: Prints the number of labels and shows the colormap. True or False
    :return: colormap for matplotlib
    """
    from matplotlib.colors import LinearSegmentedColormap
    import colorsys
    import numpy as np


    if type not in ('bright', 'soft'):
        print ('Please choose "bright" or "soft" for type')
        return

    if verbose:
        print('Number of labels: ' + str(nlabels))

    # Generate color map for bright colors, based on hsv
    if type == 'bright':
        randHSVcolors = [(np.random.uniform(low=0.0, high=1),
                          np.random.uniform(low=0.2, high=1),
                          np.random.uniform(low=0.9, high=1)) for i in range(nlabels)]

        # Convert HSV list to RGB
        randRGBcolors = []
        for HSVcolor in randHSVcolors:
            randRGBcolors.append(colorsys.hsv_to_rgb(HSVcolor[0], HSVcolor[1], HSVcolor[2]))

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]


    # Generate soft pastel colors, by limiting the RGB spectrum
    if type == 'soft':
        low = 0.6
        high = 0.95
        randRGBcolors = [(np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high)) for i in range(nlabels)]

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]


    return randRGBcolors

def get_color(series_data, cmap='Reds', lo=None, hi=None):
    from matplotlib import cm, colors
    if lo is None:
        lo = series_data.min()
    if hi is None:
        hi = series_data.max()
    if lo == hi:
        raise Exception('Invalid range.')
    color_map = cm.get_cmap(cmap, 20)
    f = lambda x: colors.rgb2hex(color_map((x-lo)/(hi-lo))[:3])
    return series_data.apply(f)

def dropdown_dataset():
    import ipywidgets as widgets
    from IPython.display import display

    w = widgets.Dropdown(
        options=['tumor+csf', 'tumor'],
        value='tumor+csf',
        description='Cohort:',
        disabled=False,
    )

    return w

def dropdown_subset():
    import ipywidgets as widgets
    from IPython.display import display

    w = widgets.Dropdown(
        options=['all', 'TcellNK','PROLIF'],
        value='TcellNK',
        description='Cell subset:',
        disabled=False,
    )

    return w

def set_box_color(bp, color):
    import matplotlib.pyplot as plt

    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color='white')
    return

def generate_paired_boxplot(celllist,allcellprop,ylabel,t):
    import matplotlib.pyplot as plt
    from scipy.stats import ttest_ind,wilcoxon
    import numpy as np

    data = [allcellprop['CSF'][c] for c in celllist]
    data2 = [allcellprop['TUMOR'][c] for c in celllist]
    for i in range(0,len(data)):
        if t == 'Ttest':
            print(celllist[i],ttest_ind(data[i],data2[i]))
        else:
            print(celllist[i],wilcoxon(data[i],data2[i]))

    bpl = plt.boxplot(data, positions=np.array(range(0,len(data)))*2.0-0.4, sym='', widths=0.6,patch_artist=True)
    bpr = plt.boxplot(data2, positions=np.array(range(0,len(data2)))*2.0+0.4, sym='', widths=0.6,patch_artist=True)
     
    set_box_color(bpl, '#0f6375')
    set_box_color(bpr, '#000000')
    for n,i in enumerate(np.array(range(0,len(data)))*2.0-0.4):
        y = data[n]
        x = np.random.normal(i, 0.04, size=len(y))
        plt.scatter(x, y, color='grey',alpha=0.7,zorder=999)
    for n,i in enumerate(np.array(range(0,len(data2)))*2.0+0.4):
        y = data2[n]
        x = np.random.normal(i, 0.04, size=len(y))
        plt.scatter(x, y, color='grey',alpha=0.7,zorder=999)
        
    plt.xticks(range(0,26,2), celllist, rotation=90)
    plt.xlim(-1,len(list(allcellprop['CSF'].keys()))*2)
    plt.ylabel(ylabel)
    plt.savefig('box.svg')
    plt.show()  