library(reticulate)
use_condaenv(condaenv="Renv", conda="/home/carlota/soft/anaconda3/bin/conda")
library(dplyr)
library(Seurat)
library(xlsx)
library(GSEABase)
library(ggplot2)

setwd("/home/carlota/projects/braimets_csf/")
results_path <- paste0("tmp/scRNAseqdata/")
cellcycle_genesets <- getGmt("data/Tirosh_cell_cycle.gmt")

###  LOAD all data 
tumormitocut <- 10
tumornfeaturecut <- 100

sample1 <- "P1"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample1,"/Tumor_filtered_feature_bc_matrix/"))
tumor1.data <- CreateSeuratObject(counts = tumor, project = sample1)
write(paste0(c(sample1,"ORIGINAL",as.character(length(tumor1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor1.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor1.data, pattern = "^MT-")
tumor1.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor1.data@meta.data$nCount_RNA)
tumor1.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor1.data$log10_nCount_RNA) + 2*sd(tumor1.data$log10_nCount_RNA)
lower_bound <- mean(tumor1.data$log10_nCount_RNA) - 2*sd(tumor1.data$log10_nCount_RNA)
tumor1.data <- subset(x = tumor1.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor1.data <- NormalizeData(object = tumor1.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor1.data <- FindVariableFeatures(object = tumor1.data, selection.method = 'vst', nfeatures = 2000)
tumor1.data@meta.data[["cancer.type"]] <- "BRCA"
tumor1.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample1,"FILTERED",as.character(length(tumor1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample2 <- "P7"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample2,"/Tumor_filtered_feature_bc_matrix/"))
tumor2.data <- CreateSeuratObject(counts = tumor, project = sample2)
write(paste0(c(sample2,"ORIGINAL",as.character(length(tumor2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor2.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor2.data, pattern = "^MT-")
tumor2.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor2.data@meta.data$nCount_RNA)
tumor2.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor2.data$log10_nCount_RNA) + 2*sd(tumor2.data$log10_nCount_RNA)
lower_bound <- mean(tumor2.data$log10_nCount_RNA) - 2*sd(tumor2.data$log10_nCount_RNA)
tumor2.data <- subset(x = tumor2.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor2.data <- NormalizeData(object = tumor2.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor2.data <- FindVariableFeatures(object = tumor2.data, selection.method = 'vst', nfeatures = 2000)
tumor2.data@meta.data[["cancer.type"]] <- "LUAD"
tumor2.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample2,"FILTERED",as.character(length(tumor2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample3 <- "P3"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample3,"/Tumor_filtered_feature_bc_matrix/"))
tumor3.data <- CreateSeuratObject(counts = tumor, project = sample3)
write(paste0(c(sample3,"ORIGINAL",as.character(length(tumor3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor3.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor3.data, pattern = "^MT-")
tumor3.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor3.data@meta.data$nCount_RNA)
tumor3.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor3.data$log10_nCount_RNA) + 2*sd(tumor3.data$log10_nCount_RNA)
lower_bound <- mean(tumor3.data$log10_nCount_RNA) - 2*sd(tumor3.data$log10_nCount_RNA)
tumor3.data <- subset(x = tumor3.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor3.data <- NormalizeData(object = tumor3.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor3.data <- FindVariableFeatures(object = tumor3.data, selection.method = 'vst', nfeatures = 2000)
tumor3.data@meta.data[["cancer.type"]] <- "LUADinf"
tumor3.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample3,"FILTERED",as.character(length(tumor3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample4 <- "P6"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample4,"/Tumor_filtered_feature_bc_matrix/"))
tumor4.data <- CreateSeuratObject(counts = tumor, project = sample4)
write(paste0(c(sample4,"ORIGINAL",as.character(length(tumor4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor4.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor4.data, pattern = "^MT-")
tumor4.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor4.data@meta.data$nCount_RNA)
tumor4.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor4.data$log10_nCount_RNA) + 2*sd(tumor4.data$log10_nCount_RNA)
lower_bound <- mean(tumor4.data$log10_nCount_RNA) - 2*sd(tumor4.data$log10_nCount_RNA)
tumor4.data <- subset(x = tumor4.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor4.data <- NormalizeData(object = tumor4.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor4.data <- FindVariableFeatures(object = tumor4.data, selection.method = 'vst', nfeatures = 2000)
tumor4.data@meta.data[["cancer.type"]] <- "LUAD"
tumor4.data@meta.data[["sample.tech"]] <- "METHANOL"
write(paste0(c(sample4,"FILTERED",as.character(length(tumor4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample5 <- 'P9'
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample5,"/Tumor_filtered_feature_bc_matrix/"))
tumor5.data <- CreateSeuratObject(counts = tumor, project = sample5)
write(paste0(c(sample5,"ORIGINAL",as.character(length(tumor5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor5.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor5.data, pattern = "^MT-")
tumor5.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor5.data@meta.data$nCount_RNA)
tumor5.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor5.data$log10_nCount_RNA) + 2*sd(tumor5.data$log10_nCount_RNA)
lower_bound <- mean(tumor5.data$log10_nCount_RNA) - 2*sd(tumor5.data$log10_nCount_RNA)
tumor5.data <- subset(x = tumor5.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor5.data <- NormalizeData(object = tumor5.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor5.data <- FindVariableFeatures(object = tumor5.data, selection.method = 'vst', nfeatures = 2000)
tumor5.data@meta.data[["cancer.type"]] <- "ESCA"
tumor5.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample5,"FILTERED",as.character(length(tumor5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample6 <- 'P12'
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample6,"/Tumor_filtered_feature_bc_matrix/"))
tumor6.data <- CreateSeuratObject(counts = tumor, project = sample6)
write(paste0(c(sample6,"ORIGINAL",as.character(length(tumor6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor6.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor6.data, pattern = "^MT-")
tumor6.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor6.data@meta.data$nCount_RNA)
tumor6.data@meta.data[["sample.type"]] <- "TUMOR"
length(tumor6.data@meta.data$orig.ident)
upper_bound <- mean(tumor6.data$log10_nCount_RNA) + 2*sd(tumor6.data$log10_nCount_RNA)
lower_bound <- mean(tumor6.data$log10_nCount_RNA) - 2*sd(tumor6.data$log10_nCount_RNA)
tumor6.data <- subset(x = tumor6.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor6.data <- NormalizeData(object = tumor6.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor6.data <- FindVariableFeatures(object = tumor6.data, selection.method = 'vst', nfeatures = 2000)
tumor6.data@meta.data[["cancer.type"]] <- "SKCM"
tumor6.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample6,"FILTERED",as.character(length(tumor6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample7 <- "P11"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample7,"/Tumor_filtered_feature_bc_matrix/"))
tumor7.data <- CreateSeuratObject(counts = tumor, project = sample7)
write(paste0(c(sample7,"ORIGINAL",as.character(length(tumor7.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor7.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor7.data, pattern = "^MT-")
tumor7.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor7.data@meta.data$nCount_RNA)
tumor7.data@meta.data[["sample.type"]] <- "TUMOR"
length(tumor7.data@meta.data$orig.ident)
upper_bound <- mean(tumor7.data$log10_nCount_RNA) + 2*sd(tumor7.data$log10_nCount_RNA)
lower_bound <- mean(tumor7.data$log10_nCount_RNA) - 2*sd(tumor7.data$log10_nCount_RNA)
tumor7.data <- subset(x = tumor7.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor7.data <- NormalizeData(object = tumor7.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor7.data <- FindVariableFeatures(object = tumor7.data, selection.method = 'vst', nfeatures = 2000)
tumor7.data@meta.data[["sample.tech"]] <- "METHANOL"
tumor7.data@meta.data[["cancer.type"]] <- "SCLC"
write(paste0(c(sample7,"FILTERED",as.character(length(tumor7.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample10 <- "P17"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample10,"/Tumor_filtered_feature_bc_matrix/"))
tumor10.data <- CreateSeuratObject(counts = tumor, project = sample10)
write(paste0(c(sample10,"ORIGINAL",as.character(length(tumor10.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor10.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor10.data, pattern = "^MT-")
tumor10.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor10.data@meta.data$nCount_RNA)
tumor10.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor10.data$log10_nCount_RNA) + 2*sd(tumor10.data$log10_nCount_RNA)
lower_bound <- mean(tumor10.data$log10_nCount_RNA) - 2*sd(tumor10.data$log10_nCount_RNA)
tumor10.data <- subset(x = tumor10.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor10.data <- NormalizeData(object = tumor10.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor10.data <- FindVariableFeatures(object = tumor10.data, selection.method = 'vst', nfeatures = 2000)
tumor10.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
tumor10.data@meta.data[["cancer.type"]] <- "BRCA"
write(paste0(c(sample10,"FILTERED",as.character(length(tumor10.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

sample12 <- "P13"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample12,"/Tumor_filtered_feature_bc_matrix/"))
tumor12.data <- CreateSeuratObject(counts = tumor, project = sample12)
write(paste0(c(sample12,"ORIGINAL",as.character(length(tumor12.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor12.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor12.data, pattern = "^MT-")
tumor12.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor12.data@meta.data$nCount_RNA)
tumor12.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor12.data$log10_nCount_RNA) + 2*sd(tumor12.data$log10_nCount_RNA)
lower_bound <- mean(tumor12.data$log10_nCount_RNA) - 2*sd(tumor12.data$log10_nCount_RNA)
tumor12.data <- subset(x = tumor12.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log12_nCount_RNA > lower_bound & log12_nCount_RNA < upper_bound)
tumor12.data <- NormalizeData(object = tumor12.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor12.data <- FindVariableFeatures(object = tumor12.data, selection.method = 'vst', nfeatures = 2000)
tumor12.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
tumor12.data@meta.data[["cancer.type"]] <- "BRCA"
write(paste0(c(sample12,"FILTERED",as.character(length(tumor12.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)


### PERFORM INTEGRATION OF SAMPLES: BATCH CORRECTION 
subset_name <- 'all'  

tumors.anchors <- FindIntegrationAnchors(object.list = list(tumor1.data,tumor2.data,tumor3.data,tumor4.data,tumor5.data,tumor6.data,tumor7.data,tumor10.data,tumor12.data), dims = 1:30) #TUMOR
tumors.integrated <- IntegrateData(anchorset = tumors.anchors, dims = 1:30)
tumors.integrated <- CellCycleScoring(tumors.integrated, s.features = cellcycle_genesets[["G1/S"]]@geneIds, g2m.features = cellcycle_genesets[["G2/M"]]@geneIds, set.ident = TRUE)

tumors.integrated <- FindVariableFeatures(object = tumors.integrated, selection.method = 'vst', nfeatures = 2000)
tumors.integrated <- ScaleData(object = tumors.integrated)
tumors.integrated <- RunPCA(object = tumors.integrated, features = VariableFeatures(object = tumors.integrated))
ElbowPlot(object = tumors.integrated, ndims = 30) + scale_y_discrete(name ="Standard Deviation", 
                                                                     limits=c("0","2","3","10")) #Asses the number of PC to be included, the ones that really explain the variability
# Identify Clusters
tumors.integrated <- FindNeighbors(tumors.integrated, dims = 1:11) 
tumors.integrated <- FindClusters(tumors.integrated, resolution = 0.4)
tumors.integrated <- RunUMAP(object = tumors.integrated, reduction = "pca", dims = 1:11)

# Generate files of interest
summary_cells_clusters <- table(tumors.integrated@meta.data$seurat_clusters, tumors.integrated@meta.data$orig.ident)
summary_cells_clusters <- cbind(summary_cells_clusters,row.names(summary_cells_clusters))
write.table(summary_cells_clusters,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_CLUSTERdist_toannotate.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

summary_cells_clusters <- table(tumorALL.merged@meta.data$seurat_clusters, tumorALL.merged@meta.data$sample.tech)
summary_cells_clusters <- cbind(summary_cells_clusters,row.names(summary_cells_clusters))
write.table(summary_cells_clusters,file=paste0(results_path,'allTUMORintegrated_SAMPLETECHdist.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)


write.table(tumors.integrated@reductions[["umap"]]@cell.embeddings,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_UMAPvalues.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)
write.table(tumors.integrated[["RNA"]]@data,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_UMI.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)

# Cell annotation 
tumor.markers <- FindAllMarkers(tumors.integrated, only.pos = TRUE, logfc.threshold = 0.25)
tumor.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC) -> top10
png(paste0(results_path,'allTUMORintegrated_',subset_name,'_cellannotations_Heatmap.png'),res = 100, height = 1200,width = 600)
DoHeatmap(object = tumors.integrated, features = top10$gene) + NoLegend()
dev.off()

tumor.markers.all <- FindAllMarkers(tumors.integrated, only.pos = FALSE, logfc.threshold = -1) 
write.table(tumor.markers.all,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_allTumorMarkers.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)


### RE-CLUSTERING 
# Choose one of the following below
subset_name <- 'TcellNK'
alltumor.combined_subset <- subset(x = tumors.integrated, subset =  seurat_clusters == 4 | seurat_clusters == 2)
# or
subset_name <- 'PROLIF'
alltumor.combined_subset <- subset(x = tumors.integrated, subset = seurat_clusters == 6)

# common code for both subsets
alltumor.combined_subset <- FindVariableFeatures(alltumor.combined_subset, selection.method = "vst", nfeatures = 2000)

#execute these parameters for TcellNK 
alltumor.combined_subset <- ScaleData(alltumor.combined_subset) 

#execute these parameters for PROLIF 
alltumor.combined_subset <- ScaleData(alltumor.combined_subset, vars.to.regress = c("G2M.Score")) 

# common code for both subsets
alltumor.combined_subset <- RunPCA(alltumor.combined_subset, features = VariableFeatures(object = alltumor.combined_subset))
ElbowPlot(object = alltumor.combined_subset, ndims = 50) + scale_y_discrete(name ="Standard Deviation", 
                                                                            limits=c("0","2","3","10")) #Asses the number of PC to be included, the ones that really explain the variability

#execute these parameters for TcellNK 
alltumor.combined_subset <- FindNeighbors(alltumor.combined_subset, dims = 1:14) 
alltumor.combined_subset <- FindClusters(alltumor.combined_subset, resolution = 0.3)
alltumor.combined_subset <- RunUMAP(object = alltumor.combined_subset, dims = 1:14)

#execute these parameters for PROLIF 
alltumor.combined_subset <- FindNeighbors(alltumor.combined_subset, dims = 1:30)
alltumor.combined_subset <- FindClusters(alltumor.combined_subset, resolution = 0.3)  
alltumor.combined_subset <- RunUMAP(object = alltumor.combined_subset, dims = 1:30)

# common code for both subsets
write.table(alltumor.combined_subset@reductions[["umap"]]@cell.embeddings,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_UMAPvalues.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)
write.table(alltumor.combined_subset[["RNA"]]@data,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_UMI.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)

summary_cells_clusters <- table(alltumor.combined_subset@meta.data$seurat_clusters, alltumor.combined_subset@meta.data$orig.ident)
summary_cells_clusters <- cbind(summary_cells_clusters,row.names(summary_cells_clusters))
write.table(summary_cells_clusters,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_CLUSTERdist_toannotate.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

# Cell annotation 
tumor.markers <- FindAllMarkers(alltumor.combined_subset, only.pos = TRUE, logfc.threshold = 0.25)
tumor.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC) -> top10
png(paste0(results_path,'allTUMORintegrated_',subset_name,'_cellannotations_Heatmap.png'),res = 100, height = 1200,width = 600)
DoHeatmap(object = alltumor.combined_subset, features = top10$gene) + NoLegend()
dev.off()

tumor.markers.all <- FindAllMarkers(alltumor.combined_subset, only.pos = FALSE, logfc.threshold = -10)
write.table(tumor.markers.all,file=paste0(results_path,'allTUMORintegrated_',subset_name,'_allTumorMarkers.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

