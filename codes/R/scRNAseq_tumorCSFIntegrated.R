library(reticulate)
use_condaenv(condaenv="Renv", conda="/home/carlota/soft/anaconda3/bin/conda")
library(dplyr)
library(Seurat)
library(xlsx)
library(GSEABase)
library(ggplot2)


setwd("/home/carlota/projects/braimets_csf/")
results_path <- paste0("tmp/scRNAseqdata/")
cellcycle_genesets <- getGmt("data/Tirosh_cell_cycle.gmt")

###  LOAD all data 
tumormitocut <- 10
tumornfeaturecut <- 100

sample1 <- "P1"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample1,"/Tumor_filtered_feature_bc_matrix/"))
tumor1.data <- CreateSeuratObject(counts = tumor, project = sample1)
write(paste0(c(sample1,"ORIGINAL",as.character(length(tumor1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor1.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor1.data, pattern = "^MT-")
tumor1.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor1.data@meta.data$nCount_RNA)
tumor1.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor1.data$log10_nCount_RNA) + 2*sd(tumor1.data$log10_nCount_RNA)
lower_bound <- mean(tumor1.data$log10_nCount_RNA) - 2*sd(tumor1.data$log10_nCount_RNA)
tumor1.data <- subset(x = tumor1.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor1.data <- NormalizeData(object = tumor1.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor1.data <- FindVariableFeatures(object = tumor1.data, selection.method = 'vst', nfeatures = 2000)
tumor1.data@meta.data[["cancer.type"]] <- "BRCA"
tumor1.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample1,"FILTERED",as.character(length(tumor1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample1,"/Liquid_filtered_feature_bc_matrix/"))
csf1.data <- CreateSeuratObject(counts = csf, project = sample1)
write(paste0(c(sample1,"ORIGINAL",as.character(length(csf1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf1.data[["percent.mt"]] <- PercentageFeatureSet(object = csf1.data, pattern = "^MT-")
csf1.data@meta.data[["log10_nCount_RNA"]] <- log10(csf1.data@meta.data$nCount_RNA)
csf1.data@meta.data[["sample.type"]] <- "CSF"
csf1.data <- subset(x = csf1.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf1.data <- NormalizeData(object = csf1.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf1.data <- FindVariableFeatures(object = csf1.data, selection.method = 'vst', nfeatures = 2000)
csf1.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample1,"FILTERED",as.character(length(csf1.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)


sample2 <- "P7"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample2,"/Tumor_filtered_feature_bc_matrix/"))
tumor2.data <- CreateSeuratObject(counts = tumor, project = sample2)
write(paste0(c(sample2,"ORIGINAL",as.character(length(tumor2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor2.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor2.data, pattern = "^MT-")
tumor2.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor2.data@meta.data$nCount_RNA)
tumor2.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor2.data$log10_nCount_RNA) + 2*sd(tumor2.data$log10_nCount_RNA)
lower_bound <- mean(tumor2.data$log10_nCount_RNA) - 2*sd(tumor2.data$log10_nCount_RNA)
tumor2.data <- subset(x = tumor2.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor2.data <- NormalizeData(object = tumor2.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor2.data <- FindVariableFeatures(object = tumor2.data, selection.method = 'vst', nfeatures = 2000)
tumor2.data@meta.data[["cancer.type"]] <- "LUAD"
tumor2.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample2,"FILTERED",as.character(length(tumor2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample2,"/Liquid_filtered_feature_bc_matrix/"))
csf2.data <- CreateSeuratObject(counts = csf, project = sample2)
write(paste0(c(sample2,"ORIGINAL",as.character(length(csf2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf2.data[["percent.mt"]] <- PercentageFeatureSet(object = csf2.data, pattern = "^MT-")
csf2.data@meta.data[["log10_nCount_RNA"]] <- log10(csf2.data@meta.data$nCount_RNA)
csf2.data@meta.data[["sample.type"]] <- "CSF"
csf2.data <- subset(x = csf2.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf2.data <- NormalizeData(object = csf2.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf2.data <- FindVariableFeatures(object = csf2.data, selection.method = 'vst', nfeatures = 2000)
csf2.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample2,"FILTERED",as.character(length(csf2.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)


sample3 <- "P3"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample3,"/Tumor_filtered_feature_bc_matrix/"))
tumor3.data <- CreateSeuratObject(counts = tumor, project = sample3)
write(paste0(c(sample3,"ORIGINAL",as.character(length(tumor3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor3.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor3.data, pattern = "^MT-")
tumor3.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor3.data@meta.data$nCount_RNA)
tumor3.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor3.data$log10_nCount_RNA) + 2*sd(tumor3.data$log10_nCount_RNA)
lower_bound <- mean(tumor3.data$log10_nCount_RNA) - 2*sd(tumor3.data$log10_nCount_RNA)
tumor3.data <- subset(x = tumor3.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor3.data <- NormalizeData(object = tumor3.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor3.data <- FindVariableFeatures(object = tumor3.data, selection.method = 'vst', nfeatures = 2000)
tumor3.data@meta.data[["cancer.type"]] <- "LUADinf"
tumor3.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample3,"FILTERED",as.character(length(tumor3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 70
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample3,"/Liquid_filtered_feature_bc_matrix/"))
csf3.data <- CreateSeuratObject(counts = csf, project = sample3)
write(paste0(c(sample3,"ORIGINAL",as.character(length(csf3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf3.data[["percent.mt"]] <- PercentageFeatureSet(object = csf3.data, pattern = "^MT-")
csf3.data@meta.data[["log10_nCount_RNA"]] <- log10(csf3.data@meta.data$nCount_RNA)
csf3.data@meta.data[["sample.type"]] <- "CSF"
csf3.data <- subset(x = csf3.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf3.data <- NormalizeData(object = csf3.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf3.data <- FindVariableFeatures(object = csf3.data, selection.method = 'vst', nfeatures = 2000)
csf3.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample3,"FILTERED",as.character(length(csf3.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)



sample4 <- "P6"
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample4,"/Tumor_filtered_feature_bc_matrix/"))
tumor4.data <- CreateSeuratObject(counts = tumor, project = sample4)
write(paste0(c(sample4,"ORIGINAL",as.character(length(tumor4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor4.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor4.data, pattern = "^MT-")
tumor4.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor4.data@meta.data$nCount_RNA)
tumor4.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor4.data$log10_nCount_RNA) + 2*sd(tumor4.data$log10_nCount_RNA)
lower_bound <- mean(tumor4.data$log10_nCount_RNA) - 2*sd(tumor4.data$log10_nCount_RNA)
tumor4.data <- subset(x = tumor4.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor4.data <- NormalizeData(object = tumor4.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor4.data <- FindVariableFeatures(object = tumor4.data, selection.method = 'vst', nfeatures = 2000)
tumor4.data@meta.data[["cancer.type"]] <- "LUAD"
tumor4.data@meta.data[["sample.tech"]] <- "METHANOL"
write(paste0(c(sample4,"FILTERED",as.character(length(tumor4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 90
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample4,"/Liquid_filtered_feature_bc_matrix/"))
csf4.data <- CreateSeuratObject(counts = csf, project = sample4)
write(paste0(c(sample4,"ORIGINAL",as.character(length(csf4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf4.data[["percent.mt"]] <- PercentageFeatureSet(object = csf4.data, pattern = "^MT-")
csf4.data@meta.data[["log10_nCount_RNA"]] <- log10(csf4.data@meta.data$nCount_RNA)
csf4.data@meta.data[["sample.type"]] <- "CSF"
csf4.data <- subset(x = csf4.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf4.data <- NormalizeData(object = csf4.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf4.data <- FindVariableFeatures(object = csf4.data, selection.method = 'vst', nfeatures = 2000)
csf4.data@meta.data[["sample.tech"]] <- "METHANOL"
write(paste0(c(sample4,"FILTERED",as.character(length(csf4.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)

sample14 <- "P6_2CSF"
csfmitocut <- 10
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample14,"/Liquid_filtered_feature_bc_matrix/"))
csf14.data <- CreateSeuratObject(counts = csf, project = sample14)
write(paste0(c(sample14,"ORIGINAL",as.character(length(csf14.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf14.data[["percent.mt"]] <- PercentageFeatureSet(object = csf14.data, pattern = "^MT-")
csf14.data@meta.data[["log10_nCount_RNA"]] <- log10(csf14.data@meta.data$nCount_RNA)
csf14.data@meta.data[["sample.type"]] <- "CSF"
csf14.data <- subset(x = csf14.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf14.data <- NormalizeData(object = csf14.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf14.data <- FindVariableFeatures(object = csf14.data, selection.method = 'vst', nfeatures = 2000)
csf14.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
csf14.data@meta.data[["cancer.type"]] <- "LUAD"
write(paste0(c(sample14,"FILTERED",as.character(length(csf14.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)

sample15 <- "P6_3CSF"
csfmitocut <- 10
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample15,"/Liquid_filtered_feature_bc_matrix/"))
csf15.data <- CreateSeuratObject(counts = csf, project = sample15)
write(paste0(c(sample15,"ORIGINAL",as.character(length(csf15.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf15.data[["percent.mt"]] <- PercentageFeatureSet(object = csf15.data, pattern = "^MT-")
csf15.data@meta.data[["log10_nCount_RNA"]] <- log10(csf15.data@meta.data$nCount_RNA)
csf15.data@meta.data[["sample.type"]] <- "CSF"
csf15.data <- subset(x = csf15.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf15.data <- NormalizeData(object = csf15.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf15.data <- FindVariableFeatures(object = csf15.data, selection.method = 'vst', nfeatures = 2000)
csf15.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
csf15.data@meta.data[["cancer.type"]] <- "LUAD"
write(paste0(c(sample15,"FILTERED",as.character(length(csf15.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)


sample5 <- 'P9'
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample5,"/Tumor_filtered_feature_bc_matrix/"))
tumor5.data <- CreateSeuratObject(counts = tumor, project = sample5)
write(paste0(c(sample5,"ORIGINAL",as.character(length(tumor5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor5.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor5.data, pattern = "^MT-")
tumor5.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor5.data@meta.data$nCount_RNA)
tumor5.data@meta.data[["sample.type"]] <- "TUMOR"
upper_bound <- mean(tumor5.data$log10_nCount_RNA) + 2*sd(tumor5.data$log10_nCount_RNA)
lower_bound <- mean(tumor5.data$log10_nCount_RNA) - 2*sd(tumor5.data$log10_nCount_RNA)
tumor5.data <- subset(x = tumor5.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor5.data <- NormalizeData(object = tumor5.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor5.data <- FindVariableFeatures(object = tumor5.data, selection.method = 'vst', nfeatures = 2000)
tumor5.data@meta.data[["cancer.type"]] <- "ESCA"
tumor5.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample5,"FILTERED",as.character(length(tumor5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample5,"/Liquid_filtered_feature_bc_matrix/"))
csf5.data <- CreateSeuratObject(counts = csf, project = sample5)
write(paste0(c(sample5,"ORIGINAL",as.character(length(csf5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf5.data[["percent.mt"]] <- PercentageFeatureSet(object = csf5.data, pattern = "^MT-")
csf5.data@meta.data[["log10_nCount_RNA"]] <- log10(csf5.data@meta.data$nCount_RNA)
csf5.data@meta.data[["sample.type"]] <- "CSF"
csf5.data <- subset(x = csf5.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf5.data <- NormalizeData(object = csf5.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf5.data <- FindVariableFeatures(object = csf5.data, selection.method = 'vst', nfeatures = 2000)
csf5.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample5,"FILTERED",as.character(length(csf5.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)

sample16 <- "P9_2CSF"
csfmitocut <- 10
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample16,"/Liquid_filtered_feature_bc_matrix/"))
csf16.data <- CreateSeuratObject(counts = csf, project = sample16)
write(paste0(c(sample16,"ORIGINAL",as.character(length(csf16.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf16.data[["percent.mt"]] <- PercentageFeatureSet(object = csf16.data, pattern = "^MT-")
csf16.data@meta.data[["log10_nCount_RNA"]] <- log10(csf16.data@meta.data$nCount_RNA)
csf16.data@meta.data[["sample.type"]] <- "CSF"
csf16.data <- subset(x = csf16.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf16.data <- NormalizeData(object = csf16.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf16.data <- FindVariableFeatures(object = csf16.data, selection.method = 'vst', nfeatures = 2000)
csf16.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
csf16.data@meta.data[["cancer.type"]] <- "ESCA"
write(paste0(c(sample16,"FILTERED",as.character(length(csf16.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)

sample13 <- "P9_3CSF"
csfmitocut <- 10
csfnfeaturecut <- 100
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample13,"/Liquid_filtered_feature_bc_matrix/"))
csf13.data <- CreateSeuratObject(counts = csf, project = sample13)
write(paste0(c(sample13,"ORIGINAL",as.character(length(csf13.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf13.data[["percent.mt"]] <- PercentageFeatureSet(object = csf13.data, pattern = "^MT-")
csf13.data@meta.data[["log10_nCount_RNA"]] <- log10(csf13.data@meta.data$nCount_RNA)
csf13.data@meta.data[["sample.type"]] <- "CSF"
csf13.data <- subset(x = csf13.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf13.data <- NormalizeData(object = csf13.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf13.data <- FindVariableFeatures(object = csf13.data, selection.method = 'vst', nfeatures = 2000)
csf13.data@meta.data[["sample.tech"]] <- "CRYOPRESERVED"
csf13.data@meta.data[["cancer.type"]] <- "ESCA"
write(paste0(c(sample13,"FILTERED",as.character(length(csf13.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)


sample6 <- 'P12'
tumor <- Read10X(data.dir = paste0("data/Table S2/",sample6,"/Tumor_filtered_feature_bc_matrix/"))
tumor6.data <- CreateSeuratObject(counts = tumor, project = sample6)
write(paste0(c(sample6,"ORIGINAL",as.character(length(tumor6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)
tumor6.data[["percent.mt"]] <- PercentageFeatureSet(object = tumor6.data, pattern = "^MT-")
tumor6.data@meta.data[["log10_nCount_RNA"]] <- log10(tumor6.data@meta.data$nCount_RNA)
tumor6.data@meta.data[["sample.type"]] <- "TUMOR"
length(tumor6.data@meta.data$orig.ident)
upper_bound <- mean(tumor6.data$log10_nCount_RNA) + 2*sd(tumor6.data$log10_nCount_RNA)
lower_bound <- mean(tumor6.data$log10_nCount_RNA) - 2*sd(tumor6.data$log10_nCount_RNA)
tumor6.data <- subset(x = tumor6.data, subset = nFeature_RNA > tumornfeaturecut & nFeature_RNA < 2500 & percent.mt < tumormitocut)# & log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
tumor6.data <- NormalizeData(object = tumor6.data, normalization.method = "LogNormalize", scale.factor = 1e4)
tumor6.data <- FindVariableFeatures(object = tumor6.data, selection.method = 'vst', nfeatures = 2000)
tumor6.data@meta.data[["cancer.type"]] <- "SKCM"
tumor6.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample6,"FILTERED",as.character(length(tumor6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allTUMORmerged_cellcounts.tsv'),append=TRUE)

csfmitocut <- 20
csfnfeaturecut <- 70
csf <- Read10X(data.dir =  paste0("data/Table S2/",sample6,"/Liquid_filtered_feature_bc_matrix/"))
csf6.data <- CreateSeuratObject(counts = csf, project = sample6)
write(paste0(c(sample6,"ORIGINAL",as.character(length(csf6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)
csf6.data[["percent.mt"]] <- PercentageFeatureSet(object = csf6.data, pattern = "^MT-")
csf6.data@meta.data[["log10_nCount_RNA"]] <- log10(csf6.data@meta.data$nCount_RNA)
csf6.data@meta.data[["sample.type"]] <- "CSF"
csf6.data <- subset(x = csf6.data, subset = nFeature_RNA > csfnfeaturecut & nFeature_RNA < 2500 & percent.mt < csfmitocut)#& log10_nCount_RNA > lower_bound & log10_nCount_RNA < upper_bound)
csf6.data <- NormalizeData(object = csf6.data, normalization.method = "LogNormalize", scale.factor = 1e4)
csf6.data <- FindVariableFeatures(object = csf6.data, selection.method = 'vst', nfeatures = 2000)
csf6.data@meta.data[["sample.tech"]] <- "FRESH"
write(paste0(c(sample6,"FILTERED",as.character(length(csf6.data$nCount_RNA))),collapse="\t"),file=paste0(results_path,'allCSFmerged_cellcounts.tsv'),append=TRUE)

### PERFORM INTEGRATION OF SAMPLES: BATCH CORRECTION 
subset_name <- 'all'  

csf.anchors <- FindIntegrationAnchors(object.list = list(csf1.data,csf2.data,csf3.data,csf4.data,csf5.data,csf6.data,csf13.data,csf14.data,csf15.data,csf16.data), dims = 1:24, k.score = 24, k.filter = 24) #CSF
csf.integrated <- IntegrateData(anchorset = csf.anchors, dims = 1:24) #CSF
csf.integrated <- CellCycleScoring(csf.integrated, s.features = cellcycle_genesets[["G1/S"]]@geneIds, g2m.features = cellcycle_genesets[["G2/M"]]@geneIds, set.ident = TRUE)

tumors.anchors <- FindIntegrationAnchors(object.list = list(tumor1.data,tumor2.data,tumor3.data,tumor4.data,tumor5.data,tumor6.data), dims = 1:30) #TUMOR
tumorscsf.integrated <- IntegrateData(anchorset = tumors.anchors, dims = 1:30) #TUMOR
tumorscsf.integrated <- CellCycleScoring(tumorscsf.integrated, s.features = cellcycle_genesets[["G1/S"]]@geneIds, g2m.features = cellcycle_genesets[["G2/M"]]@geneIds, set.ident = TRUE)

tumorscsf.anchors <- FindIntegrationAnchors(object.list = list(csf.integrated,tumorscsf.integrated))
tumorscsf.integrated<- IntegrateData(anchorset = tumorscsf.anchors)

tumorscsf.integrated <- FindVariableFeatures(object = tumorscsf.integrated, selection.method = 'vst', nfeatures = 2000)
tumorscsf.integrated <- ScaleData(object = tumorscsf.integrated)
tumorscsf.integrated <- RunPCA(object = tumorscsf.integrated, features = VariableFeatures(object = tumorscsf.integrated))
ElbowPlot(object = tumorscsf.integrated, ndims = 30) + scale_y_discrete(name ="Standard Deviation", 
                                                                        limits=c("0","2","3","10")) #Asses the number of PC to be included, the ones that really explain the variability
# Identify Clusters
tumorscsf.integrated <- FindNeighbors(tumorscsf.integrated, dims = 1:15)
tumorscsf.integrated <- FindClusters(tumorscsf.integrated, resolution = 0.4) 
tumorscsf.integrated <- RunUMAP(object = tumorscsf.integrated, reduction = "pca", dims = 1:15)

# Generate files of interest
summary_cells_clusters <- table(tumorscsf.integrated@meta.data$seurat_clusters, tumorscsf.integrated@meta.data$orig.ident)
summary_cells_clusters <- cbind(summary_cells_clusters,row.names(summary_cells_clusters))
write.table(summary_cells_clusters,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_CLUSTERdist_toannotate.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

write.table(tumorscsf.integrated@reductions[["umap"]]@cell.embeddings,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_UMAPvalues.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)
write.table(tumorscsf.integrated[["RNA"]]@data,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_UMI.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)

# Cell annotation 
tumor.markers <- FindAllMarkers(tumorscsf.integrated, only.pos = TRUE, logfc.threshold = 0.25)
tumor.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC) -> top10
png(paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_Heatmap.png'),res = 100, height = 1200,width = 600)
DoHeatmap(object = tumorscsf.integrated, features = top10$gene) + NoLegend()
dev.off()

tumor.markers.all <- FindAllMarkers(tumorscsf.integrated, only.pos = FALSE, logfc.threshold = -1) 
write.table(tumor.markers.all,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_allTumorMarkers.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)


### RE-CLUSTERING 
# Choose one of the following below
subset_name <- 'TcellNK'
tumorcsf.combined_subset <- subset(x = tumorscsf.integrated, subset =  seurat_clusters == 3 | seurat_clusters == 4)
# or
subset_name <- 'PROLIF'
tumorcsf.combined_subset <- subset(x = tumorscsf.integrated, subset = seurat_clusters == 6)

# common code for both subsets
tumorcsf.combined_subset <- FindVariableFeatures(tumorcsf.combined_subset, selection.method = "vst", nfeatures = 2000)

#execute these parameters for TcellNK 
tumorcsf.combined_subset <- ScaleData(tumorcsf.combined_subset) 

#execute these parameters for PROLIF 
tumorcsf.combined_subset <- ScaleData(tumorcsf.combined_subset, vars.to.regress = c("G2M.Score")) 

# common code for both subsets
tumorcsf.combined_subset <- RunPCA(tumorcsf.combined_subset, features = VariableFeatures(object = tumorcsf.combined_subset))
ElbowPlot(object = tumorcsf.combined_subset, ndims = 50) + scale_y_discrete(name ="Standard Deviation", 
                                                                            limits=c("0","2","3","10")) #Asses the number of PC to be included, the ones that really explain the variability

#execute these parameters for TcellNK 
tumorcsf.combined_subset <- FindNeighbors(tumorcsf.combined_subset, dims = 1:5)
tumorcsf.combined_subset <- FindClusters(tumorcsf.combined_subset, resolution = 0.3)
tumorcsf.combined_subset <- RunUMAP(object = tumorcsf.combined_subset, dims = 1:5)

#execute these parameters for PROLIF 
tumorcsf.combined_subset <- FindNeighbors(tumorcsf.combined_subset, dims = 1:8)
tumorcsf.combined_subset <- FindClusters(tumorcsf.combined_subset, resolution = 0.3)  #canviar momentani a 0.4, es 0.5
tumorcsf.combined_subset <- RunUMAP(object = tumorcsf.combined_subset, dims = 1:8)

png(paste0(results_path,subset_name,'_defaultUMAP1.png'),res = 150,width = 1200,height = 600)
p1 <- DimPlot(object = tumorcsf.combined_subset, reduction = 'umap',label = TRUE)#, pt.size = 1.5)
p2 <- DimPlot(tumorcsf.combined_subset, reduction = "umap", group.by = "orig.ident")#, pt.size = 0.1)
CombinePlots(plots = list(p1, p2))
dev.off()
CombinePlots(plots = list(p1, p2))

# common code for both subsets
write.table(tumorcsf.combined_subset@reductions[["umap"]]@cell.embeddings,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_UMAPvalues.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)
write.table(tumorcsf.combined_subset[["RNA"]]@data,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_UMI.tsv'),sep="\t",row.names = TRUE, col.names = TRUE)

summary_cells_clusters <- table(tumorcsf.combined_subset@meta.data$seurat_clusters, tumorcsf.combined_subset@meta.data$orig.ident)
summary_cells_clusters <- cbind(summary_cells_clusters,row.names(summary_cells_clusters))
write.table(summary_cells_clusters,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_CLUSTERdist_toannotate.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

# Cell annotation 
tumor.markers <- FindAllMarkers(tumorcsf.combined_subset, only.pos = TRUE, logfc.threshold = 0.25)
tumor.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC) -> top10
png(paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_cellannotations_Heatmap.png'),res = 100, height = 1200,width = 600)
DoHeatmap(object = tumorcsf.combined_subset, features = top10$gene) + NoLegend()
dev.off()

tumor.markers.all <- FindAllMarkers(tumorcsf.combined_subset, only.pos = FALSE, logfc.threshold = -10)
write.table(tumor.markers.all,file=paste0(results_path,'allTUMORCSFintegrated_',subset_name,'_allTumorMarkers.tsv'),sep="\t",row.names = FALSE, col.names = TRUE)

