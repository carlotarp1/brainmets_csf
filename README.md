# Readme #

This repository contains all codes necessary to reproduce [Rubio-Perez, Planas-Rigol, Trincado et al.](http://google.com) results. 
Extended data 1 (re-named as Table S1), 3 (re-named as Table S2) and 6 (re-named as Table S3) can be directly downloaded from the original manuscript but have also been included in this repository. 
These files are needed to be used as input files. Other required intermediate files have also been included in the data folder.



## Dependencies ##


**R 3.4.4**

R codes can be exectued as Rscript or through RStudio. Note that specific parameters need to be selected for certain data subsets, specified along the scripts with comments. Thus, a direct run of all lines is not advised. 

Library dependencies:

* reticulate
* dplyr
* Seurat
* xlsx
* GSEAbase
* fishplot
* randomcoloR


**Python 3.7.6**

All Python codes have been embeded in iPython notebooks (.ipynb extension). Thus, ipython notebook package is required to execute all codes (can be installed through Anaconconda,advised)
When distinct data subsets can be run within the same script, selection elements have been generated through iPyWidgets. Indenpdency between separate analyses within the same notebook has been granted. However, a subsequent executation of all cells is advised, to avoid data dependency issues.

Library dependencies:

* ipython
* ipython notebook
* pandas
* numpy
* sklearn
* scipy
* statsmodels
* sklearn
* seaborn
* matplotlib
* venn
* gseapy
* tqdm
* collections
* ipywidgets
* scipy

